# README #

This sample illustrates how 2 node apps can communicate via a 3rd party message queue. It is a variation of a chat application but instead of direct communication between clients, one of the services acts as the Publisher for all its clients and the other service (Broadcaster) reads messages published by the Publisher and sends them to its clients.

To run the demo locally:

1 - Run the Publisher and Broadcaster services

2 - Open one or more Publisher clients by entering http://localhost:3000/ in your browser (IE). It will ask for an ID to identify each publisher client and then open up a page where you can enter messages. If you are running multiple publishers you will see what all publishers are publishing on all clients. Messages are accumulated in the message queue until a Broadcaster is started

3 - Open one or more Broadcaster clients by entering http://localhost:3001/ in your browser (IE). It will ask for an ID to identify each broadcaster client and then open up a page where you can initiate a broadcast by pressing the Poll button. Any existing messages in the message queue will be broadcast. Any messages being inserted by the Publisher are pulled from the queue as soon as inserted. If you are running multiple broadcast clients you will see all received messages on all clients.

NOTE: to be able to run locally you will have to get the Google Cloud SDK and run the command "gcloud auth application-default login"

See this page for more info: https://cloud.google.com/sdk/gcloud/reference/auth/application-default/login"