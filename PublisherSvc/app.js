'use strict';

console.log('Publisher running');
var app = require("http").createServer(handler),
    io = require("socket.io").listen(app),
    fs = require("fs");

const projectId = 'charged-library-171714';
var gcloud = require('google-cloud')({
    projectId: projectId
    });

app.listen(3000);

// Your Google Cloud Platform project ID
var gcPubSub = gcloud.pubsub({
    projectId: projectId
});

// The name for the new topic
const topicName = 'GIG-Marbella';
// Creates the new topic
gcPubSub.createTopic(topicName)
    .then((results) => {
        const topic = results[0];
        console.log(`Topic ${topic.name} created.`);
    })
    .catch((err) => {
        console.log('ERROR:', err);
    });

gcPubSub.getTopics()
    .then((results) => {
        const topics = results[0];

        console.log('Topics:');
        topics.forEach((topic) => console.log(topic.name));
    })
    .catch((err) => {
        console.log('ERROR:', err);
    });


function publishMessage(topicName, data) {
 
    // References an existing topic, e.g. "my-topic"
    const topic = gcPubSub.topic(topicName);

    // Publishes the message, e.g. "Hello, world!" or { amount: 599.00, status: 'pending' }
    return topic.publish(data)
        .then((results) => {
            const messageIds = results[0];

            console.log(`Message ${messageIds[0]} published.`);

            return messageIds;
        });
}

function handler(req, res) {
    fs.readFile(__dirname + '/index.html',
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }
            res.writeHead(200);
            res.end(data);
        });
}

io.sockets.on('connection', function (socket) {
    //when receiving the data from the server, push the same message to client.
    socket.on("clientMsg", function (data) {
        //send the data to the current client requested/sent.
        socket.emit("serverMsg", data);
        //send the data to all the clients who are accessing the same site(localhost)
        socket.broadcast.emit("serverMsg", data);
        publishMessage(topicName,data);
    });

    socket.on("sender", function (data) {
        socket.emit("sender", data);
        socket.broadcast.emit("sender", data);
    });
});
