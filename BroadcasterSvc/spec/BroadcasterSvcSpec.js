﻿var assert = require('assert');

exports['Test 1'] = function () {
    assert.ok(true, "This shouldn't fail");
}

exports['Test 2'] = function () {
    assert.ok(1 === 1, "This shouldn't fail");
    assert.ok(false, "This should fail");
}

var request = require("request");

var base_url = "http://localhost:3001/"

describe("Broadcaster Service", function () {
    describe("GET /", function () {
        it("returns status code 200", function (done) {
            request.get(base_url, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
     });
});
