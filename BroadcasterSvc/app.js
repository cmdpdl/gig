'use strict';

console.log('Broadcaster running');
var app = require("http").createServer(handler),
    io = require("socket.io").listen(app),
    fs = require("fs");

const projectId = 'charged-library-171714';
var gcloud = require('google-cloud')({
    projectId: projectId
});

app.listen(3001);

// Your Google Cloud Platform project ID
var gcPubSub = gcloud.pubsub({
    projectId: projectId
});

// The name for the new topic
const topicName = 'GIG-Marbella';
// Creates the new topic
gcPubSub.createTopic(topicName)
    .then((results) => {
        const topic = results[0];
        console.log(`Topic ${topic.name} created.`);
    })
    .catch((err) => {
        console.log('ERROR:', err);
    });

gcPubSub.getTopics()
    .then((results) => {
        const topics = results[0];

        console.log('Topics:');
        topics.forEach((topic) => console.log(topic.name));
    })
    .catch((err) => {
        console.log('ERROR:', err);
    });

io.sockets.on('connection', function (socket) {
    //when receiving the data from the server, push the same message to client.
    socket.on("clientMsg", function (data) {
        //send the data to the current client requested/sent.
        socket.emit("serverMsg", data);
         //send the data to all the clients who are accessing the same site(localhost)
        socket.broadcast.emit("serverMsg", data);
        pullMessages(socket);
   });

    socket.on("sender", function (data) {
        socket.emit("sender", data);
        socket.broadcast.emit("sender", data);
   });
});

function pullMessages(socket) {
 
    // References an existing subscription
    const subscription = gcPubSub.subscription(topicName);

    // Pulls messages. Set returnImmediately to false to block until messages are
    // received.
    return subscription.pull({ returnImmediately: false })
            .then((results) => {
                const messages = results[0];

                console.log(`Received ${messages.length} messages.`);

                if (messages.length < 1) return false;

                messages.forEach((message) => {
                    socket.emit("serverMsg", message);
                    socket.broadcast.emit("serverMsg", message);
                    console.log(`* %d %j %j`, message.id, message.data, message.attributes);
                });

                // Acknowledges received messages. If you do not acknowledge, Pub/Sub will
                // redeliver the message.
                subscription.ack(messages.map((message) => message.ackId));
                // continue polling
                pullMessages(socket);
                return;
              });
}

function handler(req, res) {
    fs.readFile(__dirname + '/index.html',
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }
            res.writeHead(200);
            res.end(data);
        });
}